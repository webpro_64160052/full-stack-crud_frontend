export default interface Product {
  id?: number;
  login: string;
  name: string;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
